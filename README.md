 # Curso de Base de Datos


## Programa del Curso
* [Programa del Curso de Base de Datos](/resources/pdf/programa_curso_base_de_datos.pdf "Programa del Curso")

## Proyecto
* Avance  - `6 de Mayo`  [Template del Avance](/resources/proyecto/Template Avance.pdf "Template y Pauta")
 
* Presentación Final - `Desde el 17 de Junio` (PDF En proceso)


## Clases
* [Material de las clases](https://gitlab.com/brian.bastias/CIT2007-01/-/tree/main/resources/clases)


## Evaluaciones Pasadas
* [Evaluaciones](https://gitlab.com/brian.bastias/CIT2007-01/-/tree/main/resources/evaluaciones)

## Apuntes
* [¿Como instalar Postgres?](postgres_install.md)
* [Actualizar password de Postgres](postgres_update_pass.md)


## Tools
* [Draw.io](https://github.com/jgraph/drawio-desktop) - Software para modelar
* [DBeaver](https://dbeaver.io/) - UI para consultar DBs (PostgreSQL, MySQL, SQLite, Oracle, etc)
* [PGModeler](https://github.com/pgmodeler/pgmodeler) - UI para modelar y consultar DBs Postgre (crea el modelo de datos e implemta el modelo en una DB Postgre)
* [PGModeler - Instalador para Archlinux](/resources/scripts/pg_modeler/pg_modeler_install_archlinux.sh) - Instalador de PGModeler para Archlinux)
* [PGModeler - Instalador para Debian / Ubuntu o deribados](/resources/scripts/pg_modeler/pg_modeler_install_ubuntu.sh) - Instalador de PGModeler para Debian / Ubuntu y deribados)
## Bibliografia
* [Introducción a los sistemas de Base de Datos](/resources/pdf/Introduccion%20a%20los%20Sistemas%20de%20Bases%20de%20Datos%20-%207ma%20Edicion%20-%20C.%20J.%20Date.pdf "Introducción a los sistemas de Base de Datos")

* [Manual de PostgreSQL](/resources/pdf/Postgres-User.pdf "Manual de PostgreSQL")
